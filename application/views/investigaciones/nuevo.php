<main class="content">
    <div class="container-fluid p-0">
        <h1 class="h3 mb-3"><strong>Agregar</strong> Investigación</h1>
        <div class="row w-75 mx-auto">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form action="<?php echo site_url('investigaciones/guardarInvestigacion') ?>" method="POST">
                          <div class="mb-3">
                              <label class="form-label">Artículo</label>
                              <select class="form-select" name="id_art" id="id_art" required>
                                  <option value="">--Seleccionar Artículo--</option>
                                  <?php foreach ($articulos as $articulo): ?>
                                      <option value="<?php echo $articulo->id_art; ?>">
                                          <?php echo $articulo->titulo_art; ?>
                                      </option>
                                  <?php endforeach; ?>
                              </select>
                          </div>
                            <div class="mb-3">
                                <label class="form-label">Autor</label>
                                <select class="form-select" name="id_au" id="id_au" required>
                                    <option value="">--Seleccionar Autor--</option>
                                    <?php foreach ($autores as $autor): ?>
                                        <option value="<?php echo $autor->id_au; ?>">
                                            <?php echo $autor->nombre_au; ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <button type="submit" class="btn btn-primary">Guardar</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>

</main>
