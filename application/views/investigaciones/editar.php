<main class="content">
    <div class="container-fluid p-0">
        <h1 class="h3 mb-3"><strong>Editar</strong> Investigación</h1>
        <div class="row w-75 mx-auto">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form action="<?php echo site_url('investigaciones/actualizarInvestigacion') ?>" method="POST">
                            <input type="hidden" name="id_in" value="<?php echo $investigacionEditar->id_in; ?>">
                            <div class="mb-3">
                                <label class="form-label">Artículo</label>
                                <select class="form-select" name="id_art" id="id_art" required>
                                    <option value="">--Seleccionar Artículo--</option>
                                    <?php foreach ($articulos as $articulo): ?>
                                        <option value="<?php echo $articulo->id_art; ?>">
                                            <?php echo $articulo->titulo_art; ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="mb-3">
                                <label class="form-label">Autor</label>
                                <select class="form-select" name="id_au" id="id_au" required>
                                    <option value="">--Seleccionar Autor--</option>
                                    <?php foreach ($autores as $autor): ?> <!-- Cambiar $autor a $autores -->
                                        <option value="<?php echo $autor->id_au; ?>">
                                            <?php echo $autor->nombre_au; ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <button type="submit" class="btn btn-primary">Actualizar</button>
                            <a href="<?php echo site_url('investigaciones/index') ?>" class="btn btn-secondary">Cancelar</a>

                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>

</main>

<script>
    document.getElementById('id_art').value = "<?php echo $articulo->id_art; ?>";
    document.getElementById('id_au').value = "<?php echo $autor->id_au; ?>";
</script>
