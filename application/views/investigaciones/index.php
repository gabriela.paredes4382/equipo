<main class="content">
    <div class="container-fluid p-0">
        <div class="d-flex justify-content-between align-items-center mb-3">
            <h1 class="h3"><strong>Investigaciones</strong> Recientes</h1>
            <a href="<?php echo site_url('investigaciones/nuevo') ?>" class="btn btn-primary"> + Agregar</a>
        </div>

        <div class="row">
            <div class="col-12 d-flex">
                <div class="card flex-fill">
                    <?php if ($investigaciones): ?>
                        <table class="table table-hover my-0" id= "tbl_1">
                          <thead>
                              <tr>
                                  <th>ARTÍCULO</th>
                                  <th>AUTOR</th>
                                  <th>ACCIONES</th>
                              </tr>
                          </thead>
                            <tbody>
                                <?php foreach ($investigaciones as $investigacion): ?>
                                    <tr>
                                        <td>
                                            <?php
                                            foreach ($articulos as $articulo) {
                                                if ($articulo->id_art == $investigacion->fkid_ar) {
                                                    echo $articulo->titulo_art;
                                                    break;
                                                }
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            foreach ($autores as $autor) {
                                                if ($autor->id_au == $investigacion->fkid_au) {
                                                    echo $autor->nombre_au;
                                                    break;
                                                }
                                            }
                                            ?>
                                        </td>
                                        <td class="d-none d-md-table-cell">
                                            <a href="<?php echo site_url('investigaciones/editar/' . $investigacion->id_in) ?>"
                                                class="btn btn-sm btn-primary">Editar</a>
                                            <a href="javascript:void(0)"
                                                onclick="confirmarEliminar('<?php echo site_url('investigaciones/eliminar/') . $investigacion->id_in; ?>', 'Investigación');"
                                                class="btn btn-sm btn-danger">Eliminar</a>

                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php else: ?>
                        <p>No hay Investigaciones registradas</p>
                    <?php endif; ?>
                </div>
            </div>
        </div>

    </div>
</main>
</div>
</div>
<script type="text/javascript">
        $(document).ready(function() {
            $('#tbl_1').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fa-solid fa-file-pdf"></i> Exportar a PDF',
                        className: 'btn btn-outline-info',
                        messageTop: 'REPORTE DE EDITORIAL ',
                    },
                    {
                        extend: 'print',
                        text: '<i class="fa-solid fa-print"></i> Imprimir',
                        className: 'btn btn-outline-info',
                        messageTop: 'REPORTE DE EDITORIAL ',
                    },
                    {
                        extend: 'csv',
                        text: '<i class="fa-solid fa-file-csv"></i> Exportar a CSV',
                        className: 'btn btn-outline-info',
                        messageTop: 'REPORTE DE EDITORIAL ',
                    }
                ],
                language: {
                    url: "https://cdn.datatables.net/plug-ins/1.10.25/i18n/Spanish.json"
                }
            } );
        } );
    </script>
