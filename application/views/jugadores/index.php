<main class="content">
    <div class="container-fluid p-0">
        <h1><i class="mdi mdi-account-multiple menu-icon"></i> Jugadores</h1><br>
        <div class="row">
            <div class="col-md-12 text-end">
                <a href="<?php echo site_url('jugadores/nuevo') ?>" class="btn btn-outline-success">
                    <i class="fa fa-plus-circle"></i> Agregar Jugador
                </a>
            </div>
        </div>
        <?php if ($listadoJugadores): ?>
            <div class="table-responsive pt-3">
                <table class="table table-bordered" id="tbl_jugadores">
                    <thead>
                        <tr class="table-info">
                            <th>ID</th>
                            <th>APELLIDO</th>
                            <th>NOMBRE</th>
                            <th>ESTATURA</th>
                            <th>SALARIO</th>
                            <th>ESTADO</th>
                            <th>POSICIÓN</th>
                            <th>EQUIPO</th>
                            <th>ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($listadoJugadores as $jugador): ?>
                            <tr>
                                <td><?php echo $jugador->id_jug; ?></td>
                                <td><?php echo $jugador->apellido_jug; ?></td>
                                <td><?php echo $jugador->nombre_jug; ?></td>
                                <td><?php echo $jugador->estatura_jug; ?></td>
                                <td><?php echo $jugador->salario_jug; ?></td>
                                <td><?php echo $jugador->estado_jug; ?></td>
                                <td><?php echo $jugador->fk_id_pos; ?></td> 
                                <td><?php echo $jugador->fk_id_equi; ?></td> 
                                <td>
                                    <a href="<?php echo site_url('jugadores/editar/') . $jugador->id_jug; ?>" class="btn btn-warning" title="Editar">
                                        <i class="fa fa-pen"></i>
                                    </a>
                                    <a class="btn btn-danger delete-btn" href="<?php echo site_url('jugadores/borrar/') . $jugador->id_jug; ?>" title="Eliminar" data-id_jug="<?php echo $jugador->id_jug; ?>">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        <?php else: ?>
            <div class="alert alert-danger">
                NO SE ENCONTRARON JUGADORES REGISTRADOS
            </div>
        <?php endif; ?>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#tbl_jugadores').DataTable({
                    dom: 'Bfrtip',
                    buttons: [{
                            extend: 'pdfHtml5',
                            text: '<i class="fa fa-file-pdf"></i> Exportar a PDF',
                            className: 'btn btn-outline-info',
                            messageTop: 'REPORTE DE JUGADORES ',
                        },
                        {
                            extend: 'print',
                            text: '<i class="fa fa-print"></i> Imprimir',
                            className: 'btn btn-outline-info',
                            messageTop: 'REPORTE DE JUGADORES ',
                        },
                        {
                            extend: 'csv',
                            text: '<i class="fa fa-file-csv"></i> Exportar a CSV',
                            className: 'btn btn-outline-info',
                            messageTop: 'REPORTE DE JUGADORES ',
                        }
                    ],
                    language: {
                        url: "//cdn.datatables.net/plug-ins/1.10.25/i18n/Spanish.json"
                    }
                });
            });
        </script>
    </div>
</main>

<script>
    $(document).ready(function() {
        $('.delete-btn').click(function(event) {
            event.preventDefault();
            var id = $(this).data('id_jug');
            Swal.fire({
                title: '¿Está seguro de eliminar este jugador?',
                showDenyButton: true,
                showCancelButton: true,
                confirmButtonText: 'Sí',
                denyButtonText: 'No',
                customClass: {
                    actions: 'my-actions',
                    cancelButton: 'order-1 right-gap',
                    confirmButton: 'order-2',
                    denyButton: 'order-3',
                },
            }).then((result) => {
                if (result.isConfirmed) {
                    window.location.href = "<?php echo site_url('jugadores/borrar/'); ?>" + id;
                } else if (result.isDenied) {
                }
            });
        });
    });
</script>