<main class="content">
    <div class="container-fluid p-0">
        <h1><i class="mdi mdi-pencil menu-icon"></i> Editar Jugador</h1><br>
        <div class="row">
            <div class="col-md-12">
                <form method="post" action="<?php echo site_url('jugadores/actualizarJugador'); ?>" id="frm_editar_jugador">
                    <input type="hidden" name="id_jug" value="<?php echo $jugadorEditar->id_jug; ?>">
                    
                    <div class="mb-3">
                        <label for="apellido_jug" class="form-label"><b>Apellido:</b></label>
                        <input type="text" class="form-control" id="apellido_jug" name="apellido_jug" value="<?php echo $jugadorEditar->apellido_jug; ?>" required>
                        <div class="invalid-feedback">
                            Por favor ingrese el apellido del jugador.
                        </div>
                    </div>
                    
                    <div class="mb-3">
                        <label for="nombre_jug" class="form-label"><b>Nombre:</b></label>
                        <input type="text" class="form-control" id="nombre_jug" name="nombre_jug" value="<?php echo $jugadorEditar->nombre_jug; ?>" required>
                        <div class="invalid-feedback">
                            Por favor ingrese el nombre del jugador.
                        </div>
                    </div>
                    
                    <div class="mb-3">
                        <label for="estatura_jug" class="form-label"><b>Estatura:</b></label>
                        <input type="text" class="form-control" id="estatura_jug" name="estatura_jug" value="<?php echo $jugadorEditar->estatura_jug; ?>" required>
                        <div class="invalid-feedback">
                            Por favor ingrese la estatura del jugador.
                        </div>
                    </div>
                    
                    <div class="mb-3">
                        <label for="salario_jug" class="form-label"><b>Salario:</b></label>
                        <input type="text" class="form-control" id="salario_jug" name="salario_jug" value="<?php echo $jugadorEditar->salario_jug; ?>" required>
                        <div class="invalid-feedback">
                            Por favor ingrese el salario del jugador.
                        </div>
                    </div>
                    
                    <div class="mb-3">
                        <label for="estado_jug" class="form-label"><b>Estado:</b></label>
                        <input type="text" class="form-control" id="estado_jug" name="estado_jug" value="<?php echo $jugadorEditar->estado_jug; ?>" required>
                        <div class="invalid-feedback">
                            Por favor ingrese el estado del jugador.
                        </div>
                    </div>
                    
                    <div class="mb-3">
                        <label for="fk_id_pos" class="form-label"><b>Posición:</b></label>
                        <select class="form-select" id="fk_id_pos" name="fk_id_pos" required>
                            <option value="">Seleccione una posición</option>
                            <?php foreach ($posiciones as $posicion): ?>
                                <option value="<?php echo $posicion->id_pos; ?>" <?php if ($posicion->id_pos == $jugadorEditar->fk_id_pos) echo 'selected'; ?>><?php echo $posicion->nombre_pos; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <div class="invalid-feedback">
                            Por favor seleccione una posición.
                        </div>
                    </div>
                    
                    <div class="mb-3">
                        <label for="fk_id_equi" class="form-label"><b>Equipo:</b></label>
                        <select class="form-select" id="fk_id_equi" name="fk_id_equi" required>
                            <option value="">Seleccione un equipo</option>
                            <?php foreach ($equipos as $equipo): ?>
                                <option value="<?php echo $equipo->id_equi; ?>" <?php if ($equipo->id_equi == $jugadorEditar->fk_id_equi) echo 'selected'; ?>><?php echo $equipo->nombre_equi; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <div class="invalid-feedback">
                            Por favor seleccione un equipo.
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Actualizar</button>
                            <a href="<?php echo site_url('jugadores/index'); ?>" class="btn btn-danger"><i class="fa fa-times"></i> Cancelar</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            // Validación del formulario usando jQuery Validate
            $("#frm_editar_jugador").validate({
                rules: {
                    apellido_jug: {
                        required: true,
                        lettersOnly: true
                    },
                    nombre_jug: {
                        required: true,
                        lettersOnly: true
                    },
                    estatura_jug: {
                        required: true,
                        number: true
                    },
                    salario_jug: {
                        required: true,
                        number: true
                    },
                    estado_jug: {
                        required: true
                    },
                    fk_id_pos: {
                        required: true
                    },
                    fk_id_equi: {
                        required: true
                    }
                },
                messages: {
                    apellido_jug: {
                        required: "Ingrese el apellido del jugador.",
                        lettersOnly: "Ingrese solo letras."
                    },
                    nombre_jug: {
                        required: "Ingrese el nombre del jugador.",
                        lettersOnly: "Ingrese solo letras."
                    },
                    estatura_jug: {
                        required: "Ingrese la estatura del jugador.",
                        number: "Ingrese un valor numérico válido."
                    },
                    salario_jug: {
                        required: "Ingrese el salario del jugador.",
                        number: "Ingrese un valor numérico válido."
                    },
                    estado_jug: {
                        required: "Ingrese el estado del jugador."
                    },
                    fk_id_pos: {
                        required: "Seleccione una posición."
                    },
                    fk_id_equi: {
                        required: "Seleccione un equipo."
                    }
                },
                errorElement: 'div',
                errorPlacement: function(error, element) {
                    // Añadir la clase 'is-invalid' a los elementos que tienen errores
                    error.addClass('invalid-feedback');
                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.next("label"));
                    } else {
                        error.insertAfter(element);
                    }
                }
            });

            // Método personalizado para validar letras solamente
            $.validator.addMethod("lettersOnly", function(value, element) {
                return this.optional(element) || /^[a-zA-Z\s]+$/.test(value);
            }, "Ingrese solo letras.");
        });
    </script>
</main>