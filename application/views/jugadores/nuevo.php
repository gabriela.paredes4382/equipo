<main class="content">
    <div class="container-fluid p-0">
        <h2><b><i class="fa fa-plus-circle"></i> NUEVO JUGADOR</b></h2>
        <br>
        <form class="needs-validation" id="frm_nuevo_jugador" action="<?php echo site_url('jugadores/guardarJugador'); ?>" method="post" novalidate>
            <div class="mb-3">
                <label for="apellido_jug" class="form-label"><b>Apellido:</b></label>
                <input type="text" name="apellido_jug" id="apellido_jug" class="form-control" placeholder="Ingrese el Apellido" required>
            </div>
            <div class="mb-3">
                <label for="nombre_jug" class="form-label"><b>Nombre:</b></label>
                <input type="text" name="nombre_jug" id="nombre_jug" class="form-control" placeholder="Ingrese el Nombre" required>
            </div>
            <div class="mb-3">
                <label for="estatura_jug" class="form-label"><b>Estatura:</b></label>
                <input type="text" name="estatura_jug" id="estatura_jug" class="form-control" placeholder="Ingrese la Estatura" required>
            </div>
            <div class="mb-3">
                <label for="salario_jug" class="form-label"><b>Salario:</b></label>
                <input type="text" name="salario_jug" id="salario_jug" class="form-control" placeholder="Ingrese el Salario" required>
            </div>
            <div class="mb-3">
                <label for="estado_jug" class="form-label"><b>Estado:</b></label>
                <input type="text" name="estado_jug" id="estado_jug" class="form-control" placeholder="Ingrese el Estado" required>
            </div>
            <div class="mb-3">
                <label for="fk_id_pos" class="form-label"><b>Posición:</b></label>
                <select class="form-select" name="fk_id_pos" id="fk_id_pos" required>
                    <option value="">Seleccione una posición</option>
                    <?php foreach ($posiciones as $posicion): ?>
                        <option value="<?php echo $posicion->id_pos; ?>"><?php echo $posicion->nombre_pos; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="mb-3">
                <label for="fk_id_equi" class="form-label"><b>Equipo:</b></label>
                <select class="form-select" name="fk_id_equi" id="fk_id_equi" required>
                    <option value="">Seleccione un equipo</option>
                    <?php foreach ($equipos as $equipo): ?>
                        <option value="<?php echo $equipo->id_equi; ?>"><?php echo $equipo->nombre_equi; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
                    <a href="<?php echo site_url('jugadores/index'); ?>" class="btn btn-danger"><i class="fa fa-times"></i> Cancelar</a>
                </div>
            </div>
        </form>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            // Validación del formulario usando jQuery Validate
            $("#frm_nuevo_jugador").validate({
                rules: {
                    apellido_jug: {
                        required: true,
                        lettersOnly: true
                    },
                    nombre_jug: {
                        required: true,
                        lettersOnly: true
                    },
                    estatura_jug: {
                        required: true,
                        number: true
                    },
                    salario_jug: {
                        required: true,
                        number: true
                    },
                    estado_jug: {
                        required: true
                    },
                    fk_id_pos: {
                        required: true
                    },
                    fk_id_equi: {
                        required: true
                    }
                },
                messages: {
                    apellido_jug: {
                        required: "Debe ingresar el apellido del jugador.",
                        lettersOnly: "Ingrese solo letras."
                    },
                    nombre_jug: {
                        required: "Debe ingresar el nombre del jugador.",
                        lettersOnly: "Ingrese solo letras."
                    },
                    estatura_jug: {
                        required: "Debe ingresar la estatura del jugador.",
                        number: "Ingrese un valor numérico válido."
                    },
                    salario_jug: {
                        required: "Debe ingresar el salario del jugador.",
                        number: "Ingrese un valor numérico válido."
                    },
                    estado_jug: {
                        required: "Debe ingresar el estado del jugador."
                    },
                    fk_id_pos: {
                        required: "Debe seleccionar una posición."
                    },
                    fk_id_equi: {
                        required: "Debe seleccionar un equipo."
                    }
                },
                errorElement: 'div',
                errorPlacement: function(error, element) {
                    // Añadir la clase 'is-invalid' a los elementos que tienen errores
                    error.addClass('invalid-feedback');
                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.next("label"));
                    } else {
                        error.insertAfter(element);
                    }
                }
            });

            // Método personalizado para validar letras solamente
            $.validator.addMethod("lettersOnly", function(value, element) {
                return this.optional(element) || /^[a-zA-Z\s]+$/.test(value);
            }, "Ingrese solo letras.");
        });
    </script>
</main>