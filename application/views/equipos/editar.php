<main class="content">
  <div class="container-fluid p-0">
    <h1>EDITAR EQUIPO</h1>
    <form class="" id="frm_nuevo_equipo" action="<?php echo site_url('equipos/actualizarEquipo') ?>" method="post"
      enctype="multipart/form-data">
      <input type="hidden" name="id_equi" id="id_equi" value="<?php echo $equipoEditar->id_equi; ?>">
      <label for=""> <b>NOMBRE:</b> </label>
      <input type="text" name="nombre_equi" id="nombre_equi" value="<?php echo $equipoEditar->nombre_equi; ?>"
        class="form-control" placeholder="Ingrese el Nombre del Equipo" required>
      <br>
      <label for=""> <b>SIGLAS:</b> </label>
      <input type="text" name="siglas_equi" id="siglas_equi" value="<?php echo $equipoEditar->siglas_equi; ?>"
        class="form-control" placeholder="Ingrese las siglas del Equipo" required>
      <br>
      <label for=""> <b>FUNDACIÓN:</b> </label>
      <input type="text" name="fundacion_equi" id="fundacion_equi" value="<?php echo $equipoEditar->fundacion_equi; ?>"
        class="form-control" placeholder="Ingrese el año de Fundación" required>
      <br>
      <label for=""> <b>REGIÓN:</b> </label>
      <input type="text" name="region_equi" id="region_equi" value="<?php echo $equipoEditar->region_equi; ?>"
        class="form-control" placeholder="Ingrese la región del Equipo" required>
      <br>
      <label for=""> <b>NÚMERO DE TÍTULOS:</b> </label>
      <input type="text" name="numero_titulos_equi" id="numero_titulos_equi" value="<?php echo $equipoEditar->numero_titulos_equi; ?>"
        class="form-control" placeholder="Ingrese las siglas del Equipo" required>
      <br>
      <div class="row">
        <div class="col-md-12 text-center">
          <button type="submit" name="button" class="btn btn-warning"><i class="fa fa-pen"></i> &nbsp
            Actualizar</button> &nbsp &nbsp
          <a href="<?php echo site_url('equipos/index'); ?>" class="btn btn-danger"> <i class="fa fa-xmark"></i>
            &nbsp Cancelar</a>
        </div>
      </div>
    </form>
    <br>
    <br>
    <script type="text/javascript">
      $(document).ready(function () {
        $("#frm_nuevo_posicion").validate({
          rules: {
            "nombre_pos": {
              required: true,
              lettersOnly: true
            },
            "descripcion_pos": {
              required: true
            }
          },
          messages: {
            "nombre_pos": {
              required: "Debe ingresar el Nombre de la Posición"
            },
            "descripcion_pos": {
              required: "Debe ingresar la Descripción de la Posición"
            }
          }
        });

        // Agregar regla de método personalizado para letras solo
        $.validator.addMethod("lettersOnly", function (value, element) {
          return this.optional(element) || /^[a-zA-Z\s]+$/.test(value);
        }, "Ingrese solo letras");
      });
    </script>

  </div>
</main>

