<main class="content">
  <div class="container-fluid p-0">
    <h2>
      <b>
        <i class="fa fa-plus-circle"></i>
        NUEVO EQUIPO
      </b>
    </h2>
    <br>
    <form class="" id="frm_nuevo_equipo" action="<?php echo site_url('equipos/guardarEquipo') ?>" method="post"
      enctype="multipart/form-data">
      <label for=""> <b>NOMBRE:</b> </label>
      <input type="text" name="nombre_equi" id="nombre_equi" value="" class="form-control"
        placeholder="Ingrese el Nombre del Equipo" required>
      <br>
      <label for=""> <b>SIGLAS:</b> </label>
      <input type="text" name="siglas_equi" id="siglas_equi" value="" class="form-control"
        placeholder="Ingrese las siglas de Equipo" required>
      <br>
      <label for=""> <b>FUNDACIÓN:</b> </label>
      <input type="number" name="fundacion_equi" id="fundacion_equi" value="" class="form-control"
        placeholder="Ingrese el año de Fundación" required>
        <br>
      <label for=""> <b>REGIÓN:</b> </label>
      <input type="text" name="region_equi" id="region_equi" value="" class="form-control"
        placeholder="Ingrese la Región del Equipo" required>
        <br>
      <label for=""> <b>TÍTULOS:</b> </label>
      <input type="number" name="numero_titulos_equi" id="numero_titulos_equi" value="" class="form-control"
        placeholder="Ingrese el numero de títulos" required>
      <br>
      <!--Botones de guardar y cancelar -->
      <div class="row">
        <div class="col-md-12 text-center">
          <button type="submit" name="button" class="btn btn-primary"><i class="fa-regular fa-floppy-disk fa-spin"></i>
            Guardar</button>
          <a href="<?php echo site_url("equipos/index") ?>" class="btn btn-danger"><i
              class="fa-solid fa-xmark fa-spin"></i> Cancelar</a>
        </div>
      </div>
    </form>
    <script type="text/javascript">
      $(document).ready(function () {
        $("#frm_nuevo_equipo").validate({
          rules: {
            "nombre_equi": {
              required: true,
              lettersOnly: true
            },
            "siglas_equi": {
              required: true,
              lettersOnly: true
            },
            "fundacion_equi": {
              required: true
            },
            "region_equi": {
              required: true,
              lettersOnly: true
            },
            "numero_titulos_equi": {
              required: true
            }
          },
          messages: {
            "nombre_equi": {
              required: "Debe ingresar el Nombre del Equipo",
              lettersOnly: "Ingrese solo letras"
            },
            "siglas_equi": {
              required: "Debe ingresar las siglas del Equipo",
              lettersOnly: "Ingrese solo letras"
            },
            "fundacion_equi": {
              required: "Debe ingresar el año de Fundación"
            },
            "region_equi": {
              required: "Debe ingresar la región del Equipo",
              lettersOnly: "Ingrese solo letras"
            },
            "numero_titulos_equi": {
              required: "Debe ingresar el número de títulos del Equipo"
            }
          }
        });

        // Agregar regla de método personalizado para letras solo
        $.validator.addMethod("lettersOnly", function (value, element) {
          return this.optional(element) || /^[a-zA-Z\s]+$/.test(value);
        }, "Ingrese solo letras");
      });
    </script>
  </div>

</main>