
        <div class="page-wrapper">
            
            <div class="container-fluid">
                <br>
                <div class="row col-md-12 text-end">
                    <div class="col-md-6 col-8 align-self-center">
                        
            
                        </ol>
                    </div>
                    <div class="col-md-6 col-4 align-self-center">
                    <a href="<?php echo site_url('editoriales/nuevo') ?>" class="btn btn-outline-success"> <i
            class="fa fa-plus-circle"></i> Agregar Editorial</a>
                    </div>
                </div>
                <br>
                
                                    <?php if ($listadoEditorial): ?>
                                    <table class="table" id="tbl_1">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Nombre</th>
                                                <th>Correo</th>
                                                <th>Telefono</th>
                                                <th>Director</th>
                                                <th>Firma</th>
                                                <th>Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          <?php foreach ($listadoEditorial as $editorial): ?>
                                            <tr>
                                              <td><?php echo $editorial->id_ed; ?></td>
                                              <td><?php echo $editorial->nombre_ed; ?></td>
                                              <td><?php echo $editorial->correo_ed; ?></td>
                                              <td><?php echo $editorial->telefono_ed; ?></td>
                                              <td><?php echo $editorial->director_ed; ?></td>
                                              <td>
                                                  <?php if ($editorial->firma_ed!=""): ?>
                                                      <img src="<?php echo base_url('uploads/Firmas/').$editorial->firma_ed?>" style="max-width: 100px; height: auto;" alt="">
                                                  <?php else: ?>
                                                      N/A
                                                  <?php endif; ?>
                                              </td>

                                              <td>
                                                <a href="<?php echo site_url('editoriales/editar/').$editorial->id_ed; ?>" class="btn btn-warning" title="Editar"><i class="fa fa-pen"></i></a>
                                                <a class="btn btn-danger delete-btn" href="<?php echo site_url('editoriales/borrar/').$editorial->id_ed; ?>" title="Eliminar" data-id_ed="<?php echo $editorial->id_ed; ?>"><i class="fa fa-trash"></i></a>
                                              </td>
                                            </tr>
                                          <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                  <?php else: ?>
                                    <div class="alert alert-danger">
                                      No se encontró editoriales registrados
                                    </div>
                                  <?php endif; ?>
                                </div><br>

                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
            
            <script type="text/javascript">
                    $(document).ready(function() {
                        $('#tbl_1').DataTable( {
                            dom: 'Bfrtip',
                            buttons: [
                                {
                                    extend: 'pdfHtml5',
                                    text: '<i class="fa-solid fa-file-pdf"></i> Exportar a PDF',
                                    className: 'btn btn-outline-info',
                                    messageTop: 'REPORTE DE EDITORIAL ',
                                },
                                {
                                    extend: 'print',
                                    text: '<i class="fa-solid fa-print"></i> Imprimir',
                                    className: 'btn btn-outline-info',
                                    messageTop: 'REPORTE DE EDITORIAL ',
                                },
                                {
                                    extend: 'csv',
                                    text: '<i class="fa-solid fa-file-csv"></i> Exportar a CSV',
                                    className: 'btn btn-outline-info',
                                    messageTop: 'REPORTE DE EDITORIAL ',
                                }
                            ],
                            language: {
                                url: "https://cdn.datatables.net/plug-ins/1.10.25/i18n/Spanish.json"
                            }
                        } );
                    } );
                </script>
            

<script>
$(document).ready(function() {
    $('.delete-btn').click(function(event) {
        // Evitar el comportamiento predeterminado del enlace
        event.preventDefault();

        var id = $(this).data('id_ed');
        Swal.fire({
            title: '¿Está seguro de eliminar este registro?',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: 'Sí',
            denyButtonText: 'No',
            customClass: {
                actions: 'my-actions',
                cancelButton: 'order-1 right-gap',
                confirmButton: 'order-2',
                denyButton: 'order-3',
            },
        }).then((result) => {
            if (result.isConfirmed) {
                // Realizar la acción de eliminación
                // Por ejemplo, redireccionar a una URL que maneje la eliminación
                window.location.href = "<?php echo site_url('editoriales/borrar/'); ?>" + id;
                // No redirigir aquí, dejar que el servidor maneje la redirección después de eliminar
            } else if (result.isDenied) {
                // No hacer nada si el usuario cancela la eliminación
            }
        });
    });
});

</script>