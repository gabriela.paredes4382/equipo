
        <div class="page-wrapper">
            
            <div class="container-fluid">
                
               <center>
               <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center"><br>
                        <h3 class="text-themecolor m-b-0 m-t-0">Agregar Editorial</h3><br>
                        
                    </div>
                </div>
               </center>
               
                <div class="row">
                    <!-- Column -->
                    
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <center>
                    <div class="container">
                    <div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                            <div class="card-block">
                                <form id="frm_nuevo_editorial" action="<?php echo site_url('editoriales/guardarEditorial'); ?>"  enctype="multipart/form-data" method="post" class="form-horizontal form-material">
                                    <div class="form-group">
                                        <label class="col-md-12" style="text-align: left;">Nombre</label>
                                        <div class="col-md-12">
                                            <input required type="text" name="nombre_ed" id="nombre_ed" placeholder="Ingrese el nombre del editorial" class="form-control form-control-line">
                                        </div>
                                    </div>
                                    <script>
                                    document.getElementById('nombre_ed').onkeypress = function(e) {
                                      var charCode = (typeof e.which == "number") ? e.which : e.keyCode;
                                      var charTyped = String.fromCharCode(charCode);
                                      var regex = /^[A-Za-záéíóúÁÉÍÓÚ\s]+$/;
                                      if (!regex.test(charTyped) && charCode !== 8 && charCode !== 0) {
                                        e.preventDefault();
                                      }
                                    };
                                    </script><br>

                                    <div class="form-group">
                                        <label class="col-md-12" style="text-align: left;">Correo</label>
                                        <div class="col-md-12">
                                            <input required type="email" name="correo_ed" id="correo_ed" placeholder="Ingrese el correo @gmail.com" class="form-control form-control-line">
                                        </div>
                                    </div>
                                    <script>
                                        document.getElementById('correo_ed').oninput = function(e) {
                                            var inputValue = e.target.value;
                                            var regex = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/; // Expresión regular para validar correos electrónicos
                                            if (!regex.test(inputValue)) {
                                                e.target.setCustomValidity('Por favor, ingrese un correo electrónico válido');
                                            } else {
                                                e.target.setCustomValidity('');
                                            }
                                        };
                                    </script>:



                                    <div class="form-group">
                                        <label class="col-md-12" style="text-align: left;">Teléfono</label>
                                        <div class="col-md-12">
                                            <input required type="text" name="telefono_ed" id="telefono_ed" placeholder="Ingrese el telefono" class="form-control form-control-line">
                                        </div>
                                    </div>
                                    <script>
                                        document.getElementById('telefono_ed').onkeypress = function(e) {
                                            var charCode = (typeof e.which == "number") ? e.which : e.keyCode;
                                            var charTyped = String.fromCharCode(charCode);
                                            var regex = /^[0-9]*$/; // Expresión regular para permitir solo números (incluyendo el número "0")
                                            if (!regex.test(charTyped) && charCode !== 8 && charCode !== 0) {
                                                e.preventDefault();
                                            }
                                        };
                                    </script><br>

                                    <div class="form-group">
                                        <label class="col-md-12" style="text-align: left;">Director</label>
                                        <div class="col-md-12">
                                            <input required type="text" name="director_ed" id="director_ed" placeholder="Ingrese el nombre del director" class="form-control form-control-line">
                                        </div>
                                    </div>
                                    <script>
                                    document.getElementById('director_ed').onkeypress = function(e) {
                                      var charCode = (typeof e.which == "number") ? e.which : e.keyCode;
                                      var charTyped = String.fromCharCode(charCode);
                                      var regex = /^[A-Za-záéíóúÁÉÍÓÚ\s]+$/;
                                      if (!regex.test(charTyped) && charCode !== 8 && charCode !== 0) {
                                        e.preventDefault();
                                      }
                                    };
                                    </script><br>
                                    <div class="form-group">
                                        <label for="example-email" class="col-md-12" style="text-align: left;">Firma de la editorial</label>
                                        <div class="col-md-12">
                                            <input required type="file" name="firma_ed" id="firma_ed" placeholder="Ingrese la firma del editorial" class="form-control form-control-line" accept="image/*">
                                        </div>
                                    </div>    
                    </div>
                    </center>
                                    <br>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <center>
                                              <button type="submit" name="button" class="btn btn-outline-info">
                                                <i class="fa fa-save"></i>
                                                Guardar</button> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <a href="<?php echo site_url('editoriales/index'); ?>" class="btn btn-outline-danger">
                                                  <i class="fa fa-times"></i>
                                                  Cancelar</a>
                                          </center>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                
            </div>

            <script type="text/javascript">
$("#frm_nuevo_editorial").validate({
  rules:{
    "id_ed":{
      required:true
    },
    "nombre_ed":{
      required:true,
    },
    "correo_ed":{
      required:true,
    },
    "telefono_ed":{
      required:true,
    },
    "director_ed":{
      required:true,
    },
    "firma_ed":{
      required:true
    } // Agrega una coma aquí
  },
  messages:{
    "id_ed":{
      required:"Debe seleccionar el editorial"
    },
    "nombre_ed":{
      required:"Ingrese el nombre del editorial",
    },
    "correo_ed":{
      required:"Ingrese el correo",
    },
    "telefono_ed":{
      required:"Ingrese el telefono",
    },
    "director_ed":{
      required:"Ingrese la direccion",
    },
    "firma_ed":{
      required:"Seleccione la firma del editorial"
    },
  }
});
</script>