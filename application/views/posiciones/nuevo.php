<main class="content">
  <div class="container-fluid p-0">
    <h2>
      <b>
        <i class="fa fa-plus-circle"></i>
        NUEVA POSICIÓN
      </b>
    </h2>
    <br>
    <form class="" action="<?php echo site_url('posiciones/guardarPosicion') ?>" method="post"
      enctype="multipart/form-data" id="frm_nuevo_posicion" onsubmit="return validarFormulario()">
      <label for=""> <b>NOMBRE:</b> </label>
      <input type="text" name="nombre_pos" id="nombre_pos" value="" class="form-control"
        placeholder="Ingrese el nombre de la posición" required>
      <br>
      <label for=""> <b>DESCRIPCIÓN:</b> </label>
      <input type="text" name="descripcion_pos" id="descripcion_pos" value="" class="form-control"
        placeholder="Ingrese la descripción" required>
      <br>
      <!--Botones de guardar y cancelar -->
      <div class="row">
        <div class="col-md-12 text-center">
          <button type="submit" name="button" class="btn btn-primary"><i class="fa-regular fa-floppy-disk"></i>
            Guardar</button>
          <a href="<?php echo site_url("posiciones/index") ?>" class="btn btn-danger"><i
              class="fa-solid fa-xmark"></i> Cancelar</a>
        </div>
      </div>
    </form>

    <script type="text/javascript">
      $(document).ready(function () {
        $("#frm_nuevo_posicion").validate({
          rules: {
            "nombre_pos": {
              required: true,
              lettersOnly: true
            },
            "descripcion_pos": {
              required: true
            }
          },
          messages: {
            "nombre_pos": {
              required: "Debe ingresar el Nombre de la Posición"
            },
            "descripcion_pos": {
              required: "Debe ingresar la Descripción de la Posición"
            }
          }
        });

        // Agregar regla de método personalizado para letras solo
        $.validator.addMethod("lettersOnly", function (value, element) {
          return this.optional(element) || /^[a-zA-Z\s]+$/.test(value);
        }, "Ingrese solo letras");
      });
    </script>

  </div>
</main>
