<main class="content">
  <div class="container-fluid p-0">
    <h1>EDITAR POSICIONES</h1>
    <form class="" action="<?php echo site_url('posiciones/actualizarPosicion') ?>" method="post" id="frm_nuevo_posicion"
      enctype="multipart/form-data">
      <input type="hidden" name="id_pos" id="id_pos" value="<?php echo $posicionEditar->id_pos; ?>">
      <label for=""> <b>NOMBRE:</b> </label>
      <input type="text" name="nombre_pos" id="nombre_pos" value="<?php echo $posicionEditar->nombre_pos; ?>"
        class="form-control" placeholder="Ingrese el Nombre" required>
      <br>
      <label for=""> <b>DESCRIPCIÓN:</b> </label>
      <input type="text" name="descripcion_pos" id="descripcion_pos" value="<?php echo $posicionEditar->descripcion_pos; ?>" class="form-control"
        placeholder="Ingrese la Descripción" required>
      <br>
      <div class="row">
        <div class="col-md-12 text-center">
          <button type="submit" name="button" class="btn btn-warning"><i class="fa fa-pen"></i> &nbsp
            Actualizar</button> &nbsp &nbsp
          <a href="<?php echo site_url('posiciones/index'); ?>" class="btn btn-danger"> <i
              class="fa fa-xmark"></i> &nbsp Cancelar</a>
        </div>
      </div>
    </form>
    <br>
    <br>

    <script type="text/javascript">
      $(document).ready(function () {
        $("#frm_nuevo_equipo").validate({
          rules: {
            "nombre_equi": {
              required: true,
              lettersOnly: true
            },
            "siglas_equi": {
              required: true,
              lettersOnly: true
            },
            "fundacion_equi": {
              required: true
            },
            "region_equi": {
              required: true,
              lettersOnly: true
            },
            "numero_titulos_equi": {
              required: true
            }
          },
          messages: {
            "nombre_equi": {
              required: "Debe ingresar el Nombre del Equipo",
              lettersOnly: "Ingrese solo letras"
            },
            "siglas_equi": {
              required: "Debe ingresar las siglas del Equipo",
              lettersOnly: "Ingrese solo letras"
            },
            "fundacion_equi": {
              required: "Debe ingresar el año de Fundación"
            },
            "region_equi": {
              required: "Debe ingresar la región del Equipo",
              lettersOnly: "Ingrese solo letras"
            },
            "numero_titulos_equi": {
              required: "Debe ingresar el número de títulos del Equipo"
            }
          }
        });

        // Agregar regla de método personalizado para letras solo
        $.validator.addMethod("lettersOnly", function (value, element) {
          return this.optional(element) || /^[a-zA-Z\s]+$/.test(value);
        }, "Ingrese solo letras");
      });
    </script>
  </div>

</main>