<?php

class Investigacion extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function insertar($datos)
    {
        return $this->db->insert('Investigacion', $datos);
    }

    function consultarTodos()
    {
        $documentos = $this->db->get('Investigacion');
        if ($documentos->num_rows() > 0) {
            return $documentos->result();
        } else {
            return false;
        }
    }

    function obtenerPorId($id)
    {
        $this->db->where('id_in', $id);
        $documento = $this->db->get('Investigacion');
        if ($documento->num_rows() > 0) {
            return $documento->row();
        } else {
            return false;
        }
    }

    function eliminar($id)
    {

        try {
            $this->db->where('id_in', $id);
            return $this->db->delete('Investigacion');
        } catch (\Throwable $th) {
            return false;
        }

    }

    function actualizar($id, $datos)
    {
        $this->db->where('id_in', $id);
        return $this->db->update('Investigacion', $datos);
    }
}
