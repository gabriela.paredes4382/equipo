<?php
  /**
   *
   */
  class Equipo extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }//fin de la funcion constructor
    //insertar nuevo
    function insertar($datos){//$datos: es un array- conjunto de datos
      $respuesta=$this->db->insert("Equipo",$datos);//insert nos deja insertar registros permite dos parametros: la tabla y un conjunto de datos informativos como nombre, etc
      return $respuesta;
    }//fin funcion insertar
    //consulta de datos
    function consultarTodos(){
      $equipo=$this->db->get("Equipo");
      if($equipo->num_rows()>0) {
        return $equipo->result();
      } else {
        return false;
      }//fin del else
    }//fin de la funcion consulta
    //Eliminacion por id
    function eliminar($id){
      $this->db->where("id_equi",$id);
      return $this->db->delete("Equipo");
    }//fin de la funcion eliminar
    //eliminar datos
    //Consulta de uno
    function obtenerPorId($id){
      $this->db->where("id_equi",$id);
      $equipo=$this->db->get("Equipo");
      if ($equipo->num_rows()>0) {
        return $equipo->row();
      } else {
        return false;
      }
    }
    //actualizar
    function actualizar($id,$datos){
      $this->db->where("id_equi",$id);
      return $this->db->update("Equipo",$datos);
    }
  }//fin de la clase
 ?>
