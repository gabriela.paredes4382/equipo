<?php

class Respuesta extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    //consulta de datos
    function consultarTodos()
    {
        $this->db->order_by("id_res", "asc");
        $result = $this->db->get("Respuesta");
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return false;//cuando no hay datos
        }
    }

    function obtenerPorId($id)
    {
        $this->db->where("id_res", $id);
        $respuesta = $this->db->get("Respuesta");
        if ($respuesta->num_rows() > 0) {
            return $respuesta->row();
        } else {
            return false;
        }
    }
}

?>