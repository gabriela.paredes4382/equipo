<?php
  /**
   *
   */
  class Jugador extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }//fin de la funcion constructor
    //insertar nuevo
    function insertar($datos){//$datos: es un array- conjunto de datos
      $respuesta=$this->db->insert("Jugador",$datos);//insert nos deja insertar registros permite dos parametros: la tabla y un conjunto de datos informativos como nombre, etc
      return $respuesta;
    }//fin funcion insertar
    //consulta de datos
    function consultarTodos(){
      $jugador=$this->db->get("Jugador");
      if($jugador->num_rows()>0) {
        return $jugador->result();
      } else {
        return false;
      }//fin del else
    }//fin de la funcion consulta
    //Eliminacion por id
    function eliminar($id){
      $this->db->where("id_jug",$id);
      return $this->db->delete("Jugador");
    }//fin de la funcion eliminar
    //eliminar datos
    //Consulta de uno
    function obtenerPorId($id){
      $this->db->where("id_jug",$id);
      $jugador=$this->db->get("Jugador");
      if ($jugador->num_rows()>0) {
        return $jugador->row();
      } else {
        return false;
      }
    }
    //actualizar
    function actualizar($id,$datos){
      $this->db->where("id_jug",$id);
      return $this->db->update("Jugador",$datos);
    }
  }//fin de la clase
 ?>
