<?php
 
  class Editorial extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }

    //nueva editorial
    function insertar($datos){
      $respuesta=$this->db->insert("Editorial",$datos);
      return $respuesta;
    }

    //consulta de datos
    function consultarTodos(){
      $this->db->order_by("id_ed","asc");
      $result=$this->db->get("Editorial");
      if ($result->num_rows()>0) {
        return $result->result();
      } else {
        return false;//cuando no hay datos
      }
    }


    //eliminar datos
    function eliminar($id){
      $this->db->where("id_ed",$id);
      return $this->db->delete("Editorial");
    }



    //consulta de datos
    function obtenerPorId($id){
      $this->db->where("id_ed",$id);
      $editorial=$this->db->get("Editorial");
      if($editorial->num_rows()>0){
        return $editorial->row();
      } else {
        return false;
      }
    }


    function actualizar($id,$datos){
      $this->db->where("id_ed",$id);
      return $this->db->update("Editorial",$datos);
    }










  }

 ?>