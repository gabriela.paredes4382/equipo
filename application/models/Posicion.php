<?php
  /**
   *
   */
  class Posicion extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }//fin de la funcion constructor
    //insertar nuevos
    function insertar($datos){//$datos: es un array- conjunto de datos
      $respuesta=$this->db->insert("Posicion",$datos);//insert nos deja insertar registros permite dos parametros: la tabla y un conjunto de datos informativos como nombre, etc
      return $respuesta;
    }//fin funcion insertar
    //consulta de datos
    function consultarTodos(){
      $posicion=$this->db->get("Posicion");
      if($posicion->num_rows()>0) {
        return $posicion->result();
      } else {
        return false;
      }//fin del else
    }//fin de la funcion consulta
    //Eliminacion  por id
    function eliminar($id){
      $this->db->where("id_pos",$id);
      return $this->db->delete("Posicion");
    }//fin de la funcion eliminar
    //Consulta de un solo articulo
    function obtenerPorId($id){
      $this->db->where("id_pos",$id);
      $posicion=$this->db->get("Posicion");
      if ($posicion->num_rows()>0) {
        return $posicion->row();
      } else {
        return false;
      }
    }
    //actualizar HOSPITAL
    function actualizar($id,$datos){
      $this->db->where("id_pos",$id);
      return $this->db->update("Posicion",$datos);
    }
  }//fin de la clase Hospital
 ?>
