<?php
  class Posiciones extends CI_Controller
  {
    function __construct()
    {
      parent::__construct();
      $this->load->model("Posicion");
      //desabilitando errores y advertencias
      //error_reporting(0);
    }//fin de la funcionconstructor
    //renderizacion de la vista index de posiciones
    public function index(){
      $data["listadoPosiciones"]=$this->Posicion->consultarTodos();//array asociativo:data, tiene una posicion: listadoPosicion
      $this->load->view("header");
      $this->load->view("posiciones/index",$data);
      $this->load->view("footer");
    }//fin de la funcion index
    //eliminacion de articulo recibiendo el id por get
    public function borrar($id)
    {
        $this->Posicion->eliminar($id);
        $this->session->set_flashdata("confirmacion", "Posicion Eliminada Exitosamente");
        redirect("posiciones/index");
    }//fin de la funcion borrar
    //renderizacion de formulario nueva posicion
    public function nuevo(){
      $this->load->view("header");
      $this->load->view("posiciones/nuevo");
      $this->load->view("footer");
    }
    //capturando datos y e insertando en posicion
    public function guardarPosicion(){
      /*INICIO PROCESO DE SUBIDA DE ARCHIVO*/

      $datosNuevoPosicion=array(
      "nombre_pos"=>$this->input->post("nombre_pos"),
      "descripcion_pos"=>$this->input->post("descripcion_pos"));
      $this->Posicion->insertar($datosNuevoPosicion);
      $this->session->set_flashdata("confirmacion","Posicion Guardada Exitosamente"); //flash _sata crea una session de tipo flash, aparece y desaparece
      enviarEmail("bryan.figueroa1979@utc.edu.ec","Creacion","<h5> Se creo la Posicion: </h5>".$datosNuevoPosicion['nombre_pos']);
      redirect('posiciones/index');
    }
    //Renderizar el formulario de edicion
    public function editar($id){
      $data["posicionEditar"]=$this->Posicion->obtenerPorId($id);
      $this->load->view("header");
      $this->load->view("posiciones/editar",$data);
      $this->load->view("footer");
    }
    //actualizar Articulo
    public function actualizarPosicion(){
      $id_pos=$this->input->post("id_pos");
      $datosPosicion=array(
        "nombre_pos"=>$this->input->post("nombre_pos"),
        "descripcion_pos"=>$this->input->post("descripcion_pos")
      );
      $this->Posicion->actualizar($id_pos,$datosPosicion);
      $this->session->set_flashdata("confirmacion",
      "Posicion actualizada exitosamente");
      redirect('posiciones/index');
    }
  }//fin de la clase
 ?>
