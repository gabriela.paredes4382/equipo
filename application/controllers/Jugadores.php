<?php
class Jugadores extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Jugador");
        $this->load->model("Posicion");
        $this->load->model("Equipo");
    }

    public function index()
    {
        $data["listadoJugadores"] = $this->Jugador->consultarTodos();
        $this->load->view("header");
        $this->load->view("jugadores/index", $data);
        $this->load->view("footer");
    }

    public function borrar($id)
    {
        $this->Jugador->eliminar($id);
        $this->session->set_flashdata("confirmacion", "Jugador Eliminado Exitosamente");
        redirect("jugadores/index");
    }

    public function nuevo()
    {
        $data["posiciones"] = $this->Posicion->consultarTodos();
        $data["equipos"] = $this->Equipo->consultarTodos();
        $this->load->view("header");
        $this->load->view("jugadores/nuevo", $data);
        $this->load->view("footer");
    }

    //capturando datos y e insertando
    public function guardarJugador(){
        $datosNuevoJugador = array(
        "apellido_jug" => $this->input->post("apellido_jug"),
        "nombre_jug" => $this->input->post("nombre_jug"),
        "estatura_jug" => $this->input->post("estatura_jug"),
        "salario_jug" => $this->input->post("salario_jug"),
        "estado_jug" => $this->input->post("estado_jug"),
        "fk_id_pos" => $this->input->post("fk_id_pos"),
        "fk_id_equi" => $this->input->post("fk_id_equi")
        );
        $this->Jugador->insertar($datosNuevoJugador);
        $this->session->set_flashdata("confirmacion","Jugador Guardado Exitosamente"); //flash _sata crea una session de tipo flash, aparece y desaparece
        enviarEmail("bryan.figueroa1979@utc.edu.ec","Creacion","<h5> Se agrego al Jugador: </h5>".$datosNuevoJugador['nombre_jug']);
        redirect('jugadores/index');
    }
    //Renderizar el formulario de editar
    public function editar($id)
    {
        $data["jugadorEditar"] = $this->Jugador->obtenerPorId($id);
        $data["posiciones"] = $this->Posicion->consultarTodos();
        $data["equipos"] = $this->Equipo->consultarTodos();

        $this->load->view("header");
        $this->load->view("jugadores/editar", $data);
        $this->load->view("footer");
    }
    //actualizar
    public function actualizarJugador(){
        $id_jug = $this->input->post("id_jug");
        $datosJugador = array(
            "apellido_jug" => $this->input->post("apellido_jug"),
            "nombre_jug" => $this->input->post("nombre_jug"),
            "estatura_jug" => $this->input->post("estatura_jug"),
            "salario_jug" => $this->input->post("salario_jug"),
            "estado_jug" => $this->input->post("estado_jug"));
        // Actualizar los fk
        $datosJugador["fk_id_pos"] = $this->input->post("fk_id_pos");
        $datosJugador["fk_id_equi"] = $this->input->post("fk_id_equi");
        $this->Jugador->actualizar($id_jug, $datosJugador);
        $this->session->set_flashdata("confirmacion", "Jugador actualizado exitosamente");
        redirect('jugadores/index');
    }
  }//fin de la clase
 ?>
