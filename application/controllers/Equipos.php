<?php
class Equipos extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Equipo");
    }

    public function index()
    {
        $data["listadoEquipos"] = $this->Equipo->consultarTodos();
        $this->load->view("header");
        $this->load->view("equipos/index", $data);
        $this->load->view("footer");
    }

    public function borrar($id)
    {
        $this->Equipo->eliminar($id);
        $this->session->set_flashdata("confirmacion", "Equipo Eliminado Exitosamente");
        redirect("equipos/index");
    }

    public function nuevo()
    {
        $this->load->view("header");
        $this->load->view("equipos/nuevo");
        $this->load->view("footer");
    }

    //capturando datos y e insertando
    public function guardarEquipo(){
      $datosNuevoEquipo=array(
      "nombre_equi"=>$this->input->post("nombre_equi"),
      "siglas_equi"=>$this->input->post("siglas_equi"),
      "fundacion_equi"=>$this->input->post("fundacion_equi"),
      "region_equi"=>$this->input->post("region_equi"),
      "numero_titulos_equi"=>$this->input->post("numero_titulos_equi"));
      $this->Equipo->insertar($datosNuevoEquipo);
      $this->session->set_flashdata("confirmacion","Equipo Guardado Exitosamente"); //flash _sata crea una session de tipo flash, aparece y desaparece
      enviarEmail("bryan.figueroa1979@utc.edu.ec","Creacion","<h5> Se agrego al Equipo: </h5>".$datosNuevoEquipo['nombre_equi']);
      redirect('equipos/index');
    }
    //Renderizar el formulario de editar
    public function editar($id){
      $data["equipoEditar"]=$this->Equipo->obtenerPorId($id);
      $this->load->view("header");
      $this->load->view("equipos/editar",$data);
      $this->load->view("footer");
    }
    //actualizar
    public function actualizarEquipo(){
      $id_equi=$this->input->post("id_equi");
      $datosEquipo=array(
        "nombre_equi"=>$this->input->post("nombre_equi"),
        "siglas_equi"=>$this->input->post("siglas_equi"),
        "fundacion_equi"=>$this->input->post("fundacion_equi"),
        "region_equi"=>$this->input->post("region_equi"),
        "numero_titulos_equi"=>$this->input->post("numero_titulos_equi"));
      $this->Equipo->actualizar($id_equi,$datosEquipo);
      $this->session->set_flashdata("confirmacion",
      "Equipo actualizado exitosamente");
      redirect('equipos/index');
    }
  }//fin de la clase
 ?>
